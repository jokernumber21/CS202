class PrimesB
	attr_accessor :primes, :max, :neighbors

  def initialize(max)
		@max = max
	end
	
	def find_primes
		@primes = Array.new
		(0...@max).to_a.each do |n|
		  if prime?(n)
				@primes << n
			end
		end
	end
	
	def find_neighbors
		find_primes if @primes.nil?
		@neighbors = Array.new
		@primes.each_with_index do |p,i|
			if i < @max - 1
				if p + 2 == @primes[i+1]			 
					@neighbors << [p, @primes[i+1]]
				end
			end
		end
	end
	
	def count_primes
		find_primes if @primes.nil?
		return @primes.length
	end
	
	def count_neighbors
		find_neighbors if @neighbors.nil?
	  return @neighbors.length
	end
	
	def show_neighbors
	  output = "Neighbors:\n"
    cnt = 1		
		@neighbors.each do |x|
			output += "#{x}, "
		  if cnt % 4 == 0
			  output += "\n"
			end
			cnt += 1
	  end
		output += "\n"
		return output
	end
	
	def show_primes
		output = "Primes: \n"
		cnt = 1
		@primes.each do |p| 
			output += "#{p}, "
			if cnt % 5 == 0
				output += "\n"
			end
			cnt += 1
		end
		output += "\n"
	end
	
	def prime?(num)
	  if (num < 2) || (num >= @max)
	    return false
	  else
			n = 2
			until n*n > num do
				return false if num % n == 0
				if n == 2
				  n += 1
				else
					n += 2
				end
			end
	    return true
		end
	end	
	
	def to_s
    output = "Number of Primes: #{count_primes}\n"
		output += "Number of Neighbors: #{count_neighbors}\n"
	  output += "Number of Prime Neighbors: #{count_neighbors}\n"
		return output		
	end
end

if __FILE__ == $0
	puts
	puts "==== Testing PrimeB=============="
	test = PrimesB.new(10000)
	test.find_neighbors
	puts test
	puts "================================="
  test2 = PrimesB.new(20)
		(2...20).to_a.each do |x|
		print "\t#{x} - #{test2.prime?(x)}"
		puts  if x % 5 == 0
  end
  puts
  puts "================================="
	puts test2
	puts test2.show_primes
	puts test2.show_neighbors
	puts "================================="
end
