require_relative "primesC"
require 'profiler'

puts "\n==== PrimesC ==============\n"
Profiler__::start_profile

test = PrimesC.new(100000)
puts test

Profiler__::stop_profile
Profiler__::print_profile(STDOUT)