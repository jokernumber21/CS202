
require_relative "primesA"
require_relative "primesB"
require_relative "primesC"

require 'benchmark'
include Benchmark

puts "\nPrimesA Class results " + "=" * 36
bm(7) do |x|
  x.report("10"){100.times{  PrimesA.new(10).count_neighbors }}
  x.report("100"){100.times{  PrimesA.new(100).count_neighbors }}
  x.report("1000"){100.times{  PrimesA.new(1000).count_neighbors }}
  x.report("10000"){100.times{  PrimesA.new(10000).count_neighbors }}
  x.report("100000"){100.times{ PrimesA.new(100000).count_neighbors }}
	x.report("1000000"){100.times{ PrimesA.new(1000000).count_neighbors }}
end

puts "\nPrimesB Class results " + "=" * 36
bm(7) do |x|
  x.report("10"){100.times{  PrimesB.new(10).count_neighbors }}
  x.report("100"){100.times{  PrimesB.new(100).count_neighbors }}
  x.report("1000"){100.times{  PrimesB.new(1000).count_neighbors }}
  x.report("10000"){100.times{  PrimesB.new(10000).count_neighbors }}
  x.report("100000"){100.times{  PrimesB.new(100000).count_neighbors }}
end

puts "\nPrimesC Class results " + "=" * 36
bm(7) do |x|
  x.report("10"){100.times{  PrimesC.new(10).count_neighbors }}
  x.report("100"){100.times{  PrimesC.new(100).count_neighbors }}
  x.report("1000"){100.times{  PrimesC.new(1000).count_neighbors }}
  x.report("10000"){100.times{  PrimesC.new(10000).count_neighbors }}
  x.report("100000"){100.times{ PrimesC.new(100000).count_neighbors }}
	x.report("1000000"){100.times{ PrimesC.new(1000000).count_neighbors }}
end