# Experiment 1: Searching for Prime Neighbors

Comparison of 3 different class definitions for identifying prime numbers and neighboring prime numbers. 

## Classes:

Each class definition has the API with matching methods. There are some small variations in the way the attributes are used.

* **Class A:** Uses the built-in prime module of Ruby
* **Class B:** Check each number for factors by using modulus
* **Class C:** Uses a matrix of logic values to indicate if the number is prime. The matrix entries are marked for each multiple of every prime number found.

## Methods:

* **new(max)** - constructor method for a range of prime number from 2 to max - 1.
* **find_primes()** - find all prime numbers within a range
* **find_neighbors()** - find all pairs of prime neighbors
* **count_primes()** - count the number of prime numbers within the range
* **count_neighbors()** - count the number of neighboring pairs
* **show_neighbors()** - display the list of neighbor pairs
* **show_primes()** - display the list of all prime numbers
* **prime?(num)** - check if a number is a prime number or not
* **to_s()** - print a summary of the prime numbers

## Attributes:

* **@primes** - list of primes or the logic identifying if a number is prime
* **@max** - the size of the area
* **@neighbors** - list of pairs of neighboring primes 

## Experiment

1. Develop a flowchart or pseudocode to describe how the find_primes() and find_neighbors() methods work for each of the class definitions.

2. Run the makefile to  verify the flowchart and to determine how much time is spent in each method. 

3. Determine the relationship between the size of the number set tested and the time required.

4. Summary the differences between the algorithms used in the 3 classes and the impact these differences have on the processing time 

